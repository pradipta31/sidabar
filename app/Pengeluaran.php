<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $table = 'pengeluarans';
    protected $fillable = [
        'penitipan_id',
        'mutasi_id',
        'user_id',
        'tgl_pengeluaran',
        'alasan',
        'status',
        'file_pengeluaran'
    ];

    public function user(){
        return $this->belongsTo('app\User');
    }
    
    public function penitipan(){
        return $this->belongsTo('App\Penitipan');
    }

    public function mutasi(){
        return $this->belongsTo('App\Mutasi');
    }
}
