<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutasi extends Model
{
    protected $table = 'mutasis';
    protected $fillable = [
        'penitipan_id',
        'user_id',
        'tgl_mutasi',
        'keterangan',
        'status',
        'file_mutasi'
    ];

    public function user(){
        return $this->belongsTo('app\User');
    }
    
    public function penitipan(){
        return $this->belongsTo('App\Penitipan');
    }
}
