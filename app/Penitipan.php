<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penitipan extends Model
{
    protected $table = 'penitipans';
    protected $fillable = [
        'user_id',
        'golongan_id',
        'barang_id',
        'instansi_id',
        'kode_register',
        'tgl_penitipan',
        'jumlah',
        'gambar',
        'kasus',
        'file_penitipan',
        'status'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function barang(){
        return $this->belongsTo('App\Barang');
    }

    public function golongan(){
        return $this->belongsTo('App\Golongan');
    }

    public function instansi(){
        return $this->belongsTo('App\Instansi');
    }
}
