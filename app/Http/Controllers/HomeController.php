<?php

namespace App\Http\Controllers;

use DB;
use App\Barang;
use App\Instansi;
use App\Golongan;
use App\Penitipan;
use App\Mutasi;
use App\Pengeluaran;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$member = collect(DB::select('SELECT count(id) AS jumlah FROM users'))->first();
        $pen = collect(DB::select('SELECT count(id) AS jumlah FROM penitipans'))->first();
        $mut = collect(DB::select('SELECT count(id) AS jumlah FROM mutasis'))->first();
        $peng = collect(DB::select('SELECT count(id) AS jumlah FROM pengeluarans'))->first();
        $label = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        for($bulan=1;$bulan < 13;$bulan++){
            //$chartuser     = collect(DB::SELECT("SELECT count(id) AS jumlah from users where month(created_at)='$bulan'"))->first();
            //$jumlah_user[] = $chartuser->jumlah;
            $chartPen = collect(DB::SELECT("SELECT count(id) AS jumlah from penitipans WHERE month(tgl_penitipan)='$bulan'"))->first();
            $jumlahPen[] = $chartPen->jumlah;

            $chartMut = collect(DB::SELECT("SELECT count(id) AS jumlah from mutasis WHERE month(tgl_mutasi)='$bulan'"))->first();
            $jumlahMut[] = $chartMut->jumlah;

            $chartPeng = collect(DB::SELECT("SELECT count(id) AS jumlah from pengeluarans WHERE month(tgl_pengeluaran)='$bulan'"))->first();
            $jumlahPeng[] = $chartPeng->jumlah;
        }
        $countPane = [
            'barang' => Barang::all()->count(),
            'instansi' => Instansi::all()->count(),
            'golongan' => Golongan::all()->count(),
            'penitipan' => Penitipan::all()->count(),
            'mutasi' => Mutasi::all()->count(),
            'pengeluaran' => Pengeluaran::all()->count()
        ];
        return view('dashboard', compact('pen', 'mut', 'peng', 'jumlahPen', 'jumlahMut', 'jumlahPeng', 'label'), $countPane);
    }
}
