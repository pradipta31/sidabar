<?php

namespace App\Http\Controllers\Petugas;

use Image;
use Validator;
use Auth;

use App\Golongan;
use App\Barang;
use App\Instansi;
use App\Penitipan;
use App\Mutasi;
use App\Pengeluaran;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PenitipanController extends Controller
{
    public function create(){
        $instansis = Instansi::all();
        $barangs = Barang::all();
        $golongans = Golongan::all();
        return view('petugas.penitipan.create', compact('instansis', 'barangs', 'golongans'));
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTION UNTUK TAMBAH DATA PENITIPAN BARU
    |--------------------------------------------------------------------------
    |
    | Function ini berisi simpan data untuk penitipan baru
    | berisi beberapa upload gambar dan juga file penitipan
    | sudah menggunakan eloquent pada laravel.
    |
    */
    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'golongan_id' => 'required',
            'barang_id' => 'required',
            'instansi_id' => 'required',
            'tgl_penitipan' => 'required',
            'jumlah' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'kasus' => 'required',
            'file_penitipan' => 'required|mimes:pdf'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            // INPUT FILE GAMBAR
            $gambar = $r->file('gambar');
            $filename = time() . '.' . $gambar->getClientOriginalExtension();
            Image::make($gambar)->save(public_path('/images/penitipan/'.$filename));
            // INPUT FILE PDF
            $file = $r->file('file_penitipan');
            $file_penitipan = time().'.'.$file->getClientOriginalExtension();
            $path_url = 'file/penitipan';
            $file->storeAs($path_url,$file_penitipan,'public');
            
            // MAKE REGISTER CODE
            $golongan = Golongan::where('id', $r->golongan_id)->first();
            $bulan = date('m');
            $tahun = date('Y');
            $nomor = '0001';
            $kode = 'W20/RUP01/'.$golongan->kode.'/'.$bulan.'/'.$tahun.'/'.$nomor;
            
            if(Penitipan::exists()){
                // GENERATE KODE REGISTER
                $code_exists = 'W20/RUP01/'.$golongan->kode.'/'.$bulan.'/'.$tahun.'/';
                $pen = Penitipan::orderBy('id', 'DESC')->take(1)->first();
                $kode_def = $pen->kode_register;
                $sbstr = substr($kode_def, -4);
                $sbstr ++;
                $kd_register = $code_exists.sprintf("%04s", $sbstr);
                
                // INSERT DATA
                $penitipan = Penitipan::create([
                    'user_id' => Auth::user()->id,
                    'golongan_id' => $r->golongan_id,
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'kode_register' => $kd_register,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan,
                    'status' => 1
                ]);
                toastr()->success('Data penitipan baru berhasil ditambahkan dengan kode '.$kd_register);
                return redirect(url('petugas/penitipan'));
            }else{
                // INSERT DATA
                $penitipan = Penitipan::create([
                    'user_id' => Auth::user()->id,
                    'golongan_id' => $r->golongan_id,
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'kode_register' => $kode,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan,
                    'status' => 1
                ]);
                toastr()->success('Data penitipan baru berhasil ditambahkan dengan kode '.$kode);
                return redirect(url('petugas/penitipan'));
            }

            // dd($r->all());
        }
    }

    public function index(){
        $no = 1;
        $penitipans = Penitipan::all();
        return view('petugas.penitipan.index', compact('no','penitipans'));
    }

    public function edit($id){
        $instansis = Instansi::all();
        $barangs = Barang::all();
        $golongans = Golongan::all();
        $penitipan = Penitipan::findOrFail($id);
        return view('petugas.penitipan.edit', 
            compact('penitipan', 'instansis', 'barangs', 'golongans')
        );
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTION UNTUK UPDATE DATA PENITIPAN
    |--------------------------------------------------------------------------
    |
    | Function ini berisi update data untuk penitipan
    | berisi beberapa kondisi untuk upload gambar maupun file penitipan
    | sudah menggunakan eloquent pada laravel.
    |
    */
    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'barang_id' => 'required',
            'instansi_id' => 'required',
            'tgl_penitipan' => 'required',
            'jumlah' => 'required',
            'kasus' => 'required',
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $check_pen = Penitipan::where('id', $id)->first();
            // KONDISI CEK APAKAH ADA PENGINPUTAN FILE PENITIPAN
            if ($r->file('file_penitipan')) {
                $file = $r->file('file_penitipan');
                $file_penitipan = time().'.'.$file->getClientOriginalExtension();
                $path_url = 'file/penitipan';
                // unlink(storage_path('app/public/file/penitipan/').$check_pen->file_penitipan);
                $file->storeAs($path_url,$file_penitipan,'public');
            }else{
                $file_penitipan = null;
            }
            // KONDISI CEK APAKAH ADA PENGINPUTAN GAMBAR
            if ($r->file('gambar')) {
                $gambar = $r->file('gambar');
                $filename = time() . '.' . $gambar->getClientOriginalExtension();
                // unlink(public_path('/images/penitipan/').$check_pen->gambar);
                Image::make($gambar)->save(public_path('/images/penitipan/'.$filename));
            }else{
                $filename = null;
            }

            // QUERY
            // KONDISI GAMBAR DAN FILE PENITIPAN KOSONG
            if ($filename == null && $file_penitipan == null) {
                // dd('Gambar null, File NULL');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'kasus' => $r->kasus
                ]);
            }
            // KONDISI GAMBAR DAN FILE PENITIPAN ISI
            elseif($filename != null && $file_penitipan != null){
                // dd('Gambar isi, File isi');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan
                ]);
            }
            // KONDISI GAMBAR KOSONG dan FILE PENITIPAN ISI
            elseif($filename == null && $file_penitipan != null){
                // dd('Gambar null, File isi');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan
                ]);
            }
            // KONDISI GAMBAR ISI dan FILE PENITIPAN KOSONG
            elseif($filename != null && $file_penitipan == null){
                // dd('Gambar isi, File NULL');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'nama_barang' => $r->nama_barang,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus
                ]);
            }

            toastr()->success('Data penitipan berhasil diubah!');
            return redirect(url('petugas/penitipan'));
        }
    }

    public function detail($id){
        $penitipan = Penitipan::findOrFail($id);
        return view('petugas.penitipan.detail', compact('penitipan'));
    }

    public function downloadFile($get_penitipan){
        return response()->download(base_path('storage/app/public/file/penitipan/'.$get_penitipan));
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTION UNTUK MUTASI dan KELUARAN PENITIPAN BARANG
    |--------------------------------------------------------------------------
    |
    */

    public function mutasiPenitipan($id){
        $pen = Penitipan::findOrFail($id);
        return view('petugas.penitipan.mutasi', compact('pen'));
    }

    public function keluarPenitipan($id){
        $pen = Penitipan::findOrFail($id);
        return view('petugas.penitipan.keluar', compact('pen'));
    }

    public function destroy($id){
        $penitipan = Penitipan::where('id', $id)->first();
        unlink(public_path('/images/penitipan/').$penitipan->gambar);
        unlink(storage_path('app/public/file/penitipan/').$penitipan->file_penitipan);
        $penitipan->delete();
        $mutasi = Mutasi::where('penitipan_id', $id)->first();
        if ($mutasi != NULL) {
            unlink(storage_path('app/public/file/mutasi/').$mutasi->file_mutasi);
            $mutasi->delete();
        }
        $pengeluaran = Pengeluaran::where('penitipan_id', $id)->first();
        if ($pengeluaran != NULL) {
            unlink(storage_path('app/public/file/pengeluaran/').$mutasi->file_pengeluaran);
            $pengeluaran->delete();
        }
        toastr()->success('Data penitipan telah berhasil dihapus!');
        return redirect(url('admin/penitipan'));
    }
}
