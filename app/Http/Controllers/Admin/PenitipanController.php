<?php

namespace App\Http\Controllers\Admin;

use Image;
use Validator;
use Auth;
// RELATION
use App\Golongan;
use App\Barang;
use App\Instansi;

use App\User;
use App\Penitipan;
use App\Mutasi;
use App\Pengeluaran;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PenitipanController extends Controller
{

    public function create(){
        $instansis = Instansi::all();
        $barangs = Barang::all();
        $golongans = Golongan::all();
        return view('admin.penitipan.create', 
            compact('instansis', 'barangs', 'golongans')
        );
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTION UNTUK TAMBAH DATA PENITIPAN BARU
    |--------------------------------------------------------------------------
    |
    | Function ini berisi simpan data untuk penitipan baru
    | berisi beberapa upload gambar dan juga file penitipan
    | sudah menggunakan eloquent pada laravel.
    |
    */
    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'golongan_id' => 'required',
            'barang_id' => 'required',
            'instansi_id' => 'required',
            'tgl_penitipan' => 'required',
            'jumlah' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'kasus' => 'required',
            'file_penitipan' => 'required|mimes:pdf'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            // INPUT FILE GAMBAR
            $gambar = $r->file('gambar');
            $filename = time() . '.' . $gambar->getClientOriginalExtension();
            Image::make($gambar)->save(public_path('/images/penitipan/'.$filename));
            // INPUT FILE PDF
            $file = $r->file('file_penitipan');
            $file_penitipan = time().'.'.$file->getClientOriginalExtension();
            $path_url = 'file/penitipan';
            $file->storeAs($path_url,$file_penitipan,'public');
            
            // MAKE REGISTER CODE
            $golongan = Golongan::where('id', $r->golongan_id)->first();
            $bulan = date('m');
            $tahun = date('Y');
            $nomor = '0001';
            $kode = 'W20/RUP01/'.$golongan->kode.'/'.$bulan.'/'.$tahun.'/'.$nomor;
            
            if(Penitipan::exists()){
                // GENERATE KODE REGISTER
                $code_exists = 'W20/RUP01/'.$golongan->kode.'/'.$bulan.'/'.$tahun.'/';
                $pen = Penitipan::orderBy('id', 'DESC')->take(1)->first();
                $kode_def = $pen->kode_register;
                $sbstr = substr($kode_def, -4);
                $sbstr ++;
                $kd_register = $code_exists.sprintf("%04s", $sbstr);
                
                // INSERT DATA
                $penitipan = Penitipan::create([
                    'user_id' => Auth::user()->id,
                    'golongan_id' => $r->golongan_id,
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'kode_register' => $kd_register,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan,
                    'status' => 1
                ]);
                toastr()->success('Data penitipan baru berhasil ditambahkan dengan kode '.$kd_register);
                return redirect(url('admin/penitipan'));
            }else{
                // INSERT DATA
                $penitipan = Penitipan::create([
                    'user_id' => Auth::user()->id,
                    'golongan_id' => $r->golongan_id,
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'kode_register' => $kode,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan,
                    'status' => 1
                ]);
                toastr()->success('Data penitipan baru berhasil ditambahkan dengan kode '.$kode);
                return redirect(url('admin/penitipan'));
            }

            // dd($r->all());
        }
    }

    public function index(){
        $no = 1;
        $penitipans = Penitipan::all();
        return view('admin.penitipan.index', compact('no','penitipans'));
    }

    public function edit($id){
        $instansis = Instansi::all();
        $barangs = Barang::all();
        $golongans = Golongan::all();
        $penitipan = Penitipan::where('id',$id)->first();
        if($penitipan != null){
            return view('admin.penitipan.edit', 
            compact('penitipan', 'instansis', 'barangs', 'golongans')
        );
        }else{
            abort('404');
        }
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTION UNTUK UPDATE DATA PENITIPAN
    |--------------------------------------------------------------------------
    |
    | Function ini berisi update data untuk penitipan
    | berisi beberapa kondisi untuk upload gambar maupun file penitipan
    | sudah menggunakan eloquent pada laravel.
    |
    */
    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'barang_id' => 'required',
            'instansi_id' => 'required',
            'tgl_penitipan' => 'required',
            'jumlah' => 'required',
            'kasus' => 'required',
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $check_pen = Penitipan::where('id', $id)->first();
            // KONDISI CEK APAKAH ADA PENGINPUTAN FILE PENITIPAN
            if ($r->file('file_penitipan')) {
                $file = $r->file('file_penitipan');
                $file_penitipan = time().'.'.$file->getClientOriginalExtension();
                $path_url = 'file/penitipan';
                // unlink(storage_path('app/public/file/penitipan/').$check_pen->file_penitipan);
                $file->storeAs($path_url,$file_penitipan,'public');
            }else{
                $file_penitipan = null;
            }
            // KONDISI CEK APAKAH ADA PENGINPUTAN GAMBAR
            if ($r->file('gambar')) {
                $gambar = $r->file('gambar');
                $filename = time() . '.' . $gambar->getClientOriginalExtension();
                // unlink(public_path('/images/penitipan/').$check_pen->gambar);
                Image::make($gambar)->save(public_path('/images/penitipan/'.$filename));
            }else{
                $filename = null;
            }

            // QUERY
            // KONDISI GAMBAR DAN FILE PENITIPAN KOSONG
            if ($filename == null && $file_penitipan == null) {
                // dd('Gambar null, File NULL');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'kasus' => $r->kasus
                ]);
            }
            // KONDISI GAMBAR DAN FILE PENITIPAN ISI
            elseif($filename != null && $file_penitipan != null){
                // dd('Gambar isi, File isi');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan
                ]);
            }
            // KONDISI GAMBAR KOSONG dan FILE PENITIPAN ISI
            elseif($filename == null && $file_penitipan != null){
                // dd('Gambar null, File isi');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'jumlah' => $r->jumlah,
                    'kasus' => $r->kasus,
                    'file_penitipan' => $file_penitipan
                ]);
            }
            // KONDISI GAMBAR ISI dan FILE PENITIPAN KOSONG
            elseif($filename != null && $file_penitipan == null){
                // dd('Gambar isi, File NULL');
                $check_pen->update([
                    'barang_id' => $r->barang_id,
                    'instansi_id' => $r->instansi_id,
                    'tgl_penitipan' => $r->tgl_penitipan,
                    'nama_barang' => $r->nama_barang,
                    'jumlah' => $r->jumlah,
                    'gambar' => $filename,
                    'kasus' => $r->kasus
                ]);
            }

            toastr()->success('Data penitipan berhasil diubah!');
            return redirect(url('admin/penitipan'));
        }
    }

    public function detail($id){
        $penitipan = Penitipan::where('id',$id)->first();
        if($penitipan != null){
            return view('admin.penitipan.detail', compact('penitipan'));
        }else{
            abort('404');
        }
    }

    public function downloadFile($get_penitipan){
        return response()->download(base_path('storage/app/public/file/penitipan/'.$get_penitipan));
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTION UNTUK MUTASI dan KELUARAN PENITIPAN BARANG
    |--------------------------------------------------------------------------
    |
    */

    public function mutasiPenitipan($id){
        $pen = Penitipan::findOrFail($id);
        return view('admin.penitipan.mutasi', compact('pen'));
    }

    public function keluarPenitipan($id){
        $pen = Penitipan::findOrFail($id);
        return view('admin.penitipan.keluar', compact('pen'));
    }

    public function destroy($id){
        $penitipan = Penitipan::where('id', $id)->first();
        unlink(public_path('/images/penitipan/').$penitipan->gambar);
        unlink(storage_path('app/public/file/penitipan/').$penitipan->file_penitipan);
        $penitipan->delete();
        $mutasi = Mutasi::where('penitipan_id', $id)->first();
        if ($mutasi != NULL) {
            unlink(storage_path('app/public/file/mutasi/').$mutasi->file_mutasi);
            $mutasi->delete();
        }
        $pengeluaran = Pengeluaran::where('penitipan_id', $id)->first();
        if ($pengeluaran != NULL) {
            unlink(storage_path('app/public/file/pengeluaran/').$mutasi->file_pengeluaran);
            $pengeluaran->delete();
        }
        toastr()->success('Data penitipan telah berhasil dihapus!');
        return redirect(url('admin/penitipan'));
    }

    public function cetak(Request $r){
        $validator = Validator::make($r->all(), [
            'jenis' => 'required',
            'date_1' => 'required',
            'date_2' => 'required'
        ]);
        if ($validator->fails()) {
            toastError('Semua inputan harus diisi!');
            return redirect()->back()->withInput();
        }else{
            $from = $r->date_1;
            $to = $r->date_2;
            $penitipans = Penitipan::whereBetween('tgl_penitipan', [$from, $to])->get();
            $mutasis = Mutasi::whereBetween('tgl_mutasi', [$from, $to])->get();
            $pengeluarans = Pengeluaran::whereBetween('tgl_pengeluaran', [$from, $to])->get();
            
            $spreadsheet = new Spreadsheet;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'No');
            $sheet->setCellValue('B1', 'User Penginputan');
            $sheet->setCellValue('C1', 'Golongan');
            $sheet->setCellValue('D1', 'Nama Barang');
            $sheet->setCellValue('E1', 'Instansi');
            $sheet->setCellValue('F1', 'Kode Register');
            $sheet->setCellValue('G1', 'Tanggal Penitipan');
            $sheet->setCellValue('H1', 'Tanggal Mutasi');
            $sheet->setCellValue('I1', 'Tanggal Keluar');
            $sheet->setCellValue('J1', 'Jumlah');
            $sheet->setCellValue('K1', 'Gambar');
            $sheet->setCellValue('L1', 'Kasus');
            $sheet->setCellValue('M1', 'File Penitipan');
            $sheet->setCellValue('N1', 'Status');
            $sheet->setCellValue('O1', 'Tanggal Input');

            $row = 2;
            $nomor = 1;
            if ($r->jenis == 'penitipan') {
                foreach($penitipans as $penitipan){
                    $status = '';
                    if($penitipan->status == 1){
                        $status = 'Penitipan';
                    }elseif($penitipan->status == 2) {
                        $status = 'Termutasi';
                    }else{
                        $status = 'Dikeluarkan';
                    }
                    $sheet->setCellValue('A'.$row,$nomor++);
                    $sheet->setCellValue('B'.$row,$penitipan->user->nama);
                    $sheet->setCellValue('C'.$row,$penitipan->golongan->kode."-".$penitipan->golongan->nama_golongan);
                    $sheet->setCellValue('D'.$row,$penitipan->barang->nama_barang);
                    $sheet->setCellValue('E'.$row,$penitipan->instansi->nama_instansi);
                    $sheet->setCellValue('F'.$row,$penitipan->kode_register);
                    $sheet->setCellValue('G'.$row,date('d-m-Y', strtotime($penitipan->tgl_penitipan)));
                    $sheet->setCellValue('J'.$row,$penitipan->jumlah);
                    $sheet->setCellValue('K'.$row,'Ada');
                    $sheet->setCellValue('L'.$row,substr($penitipan->kasus, 3, -4));
                    $sheet->setCellValue('M'.$row,'Ada');
                    $sheet->setCellValue('N'.$row,$status);
                    $sheet->setCellValue('O'.$row,date('d-m-Y', strtotime($penitipan->created_at)));
                    $row++;
                }
                $writer = new Xlsx($spreadsheet);
                $writer->save('PenitipanCetak.xlsx');
                return response()->download(public_path('PenitipanCetak.xlsx'))->deleteFileAfterSend();
            }else if($r->jenis == 'mutasi'){
                foreach($mutasis as $mutasi){
                    $status = '';
                    if($mutasi->status == 1){
                        $status = 'Penitipan';
                    }elseif($mutasi->status == 2) {
                        $status = 'Termutasi';
                    }else{
                        $status = 'Dikeluarkan';
                    }
                    $sheet->setCellValue('A'.$row,$nomor++);
                    $sheet->setCellValue('B'.$row,$mutasi->user->nama);
                    $sheet->setCellValue('C'.$row,$mutasi->penitipan->golongan->kode."-".$mutasi->penitipan->golongan->nama_golongan);
                    $sheet->setCellValue('D'.$row,$mutasi->penitipan->barang->nama_barang);
                    $sheet->setCellValue('E'.$row,$mutasi->penitipan->instansi->nama_instansi);
                    $sheet->setCellValue('F'.$row,$mutasi->penitipan->kode_register);
                    $sheet->setCellValue('G'.$row,date('d-m-Y', strtotime($mutasi->penitipan->tgl_penitipan)));
                    $sheet->setCellValue('H'.$row,date('d-m-Y', strtotime($mutasi->tgl_mutasi)));
                    $sheet->setCellValue('J'.$row,$mutasi->penitipan->jumlah);
                    $sheet->setCellValue('K'.$row,'Ada');
                    $sheet->setCellValue('L'.$row,substr($mutasi->penitipan->kasus, 3, -4));
                    $sheet->setCellValue('M'.$row,'Ada');
                    $sheet->setCellValue('N'.$row,$status);
                    $sheet->setCellValue('O'.$row,date('d-m-Y', strtotime($mutasi->created_at)));
                    $row++;
                }
                $writer = new Xlsx($spreadsheet);
                $writer->save('MutasiCetak.xlsx');
                return response()->download(public_path('MutasiCetak.xlsx'))->deleteFileAfterSend();
            }else if($r->jenis == 'keluar'){
                foreach($pengeluarans as $pengeluaran){
                    $status = '';
                    if($pengeluaran->status == 1){
                        $status = 'Penitipan';
                    }elseif($pengeluaran->status == 2) {
                        $status = 'Termutasi';
                    }else{
                        $status = 'Dikeluarkan';
                    }
                    $sheet->setCellValue('A'.$row,$nomor++);
                    $sheet->setCellValue('B'.$row,$pengeluaran->user->nama);
                    $sheet->setCellValue('C'.$row,$pengeluaran->penitipan->golongan->kode."-".$pengeluaran->penitipan->golongan->nama_golongan);
                    $sheet->setCellValue('D'.$row,$pengeluaran->penitipan->barang->nama_barang);
                    $sheet->setCellValue('E'.$row,$pengeluaran->penitipan->instansi->nama_instansi);
                    $sheet->setCellValue('F'.$row,$pengeluaran->penitipan->kode_register);
                    $sheet->setCellValue('G'.$row,date('d-m-Y', strtotime($pengeluaran->penitipan->tgl_penitipan)));
                    if($pengeluaran->mutasi_id != null){
                        $sheet->setCellValue('H'.$row,date('d-m-Y', strtotime($pengeluaran->mutasi->tgl_mutasi)));
                    }else{
                        
                    }
                    $sheet->setCellValue('I'.$row,date('d-m-Y', strtotime($pengeluaran->tgl_pengeluaran)));
                    $sheet->setCellValue('J'.$row,$pengeluaran->penitipan->jumlah);
                    $sheet->setCellValue('K'.$row,'Ada');
                    $sheet->setCellValue('L'.$row,substr($pengeluaran->penitipan->kasus, 3, -4));
                    $sheet->setCellValue('M'.$row,'Ada');
                    $sheet->setCellValue('N'.$row,$status);
                    $sheet->setCellValue('O'.$row,date('d-m-Y', strtotime($pengeluaran->created_at)));
                    $row++;
                }
                $writer = new Xlsx($spreadsheet);
                $writer->save('MutasiCetak.xlsx');
                return response()->download(public_path('MutasiCetak.xlsx'))->deleteFileAfterSend();
            }
        }
    }
}
