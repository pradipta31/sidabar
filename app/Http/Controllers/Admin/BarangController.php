<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Barang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index(){
        $no = 1;
        $barangs = Barang::all();
        return view('admin.barang.index', compact('no','barangs'));
    }

    public function store(Request $r){
        $v = Validator::make($r->all(),[
            'jenis_barang' => 'required',
            'nama_barang' => 'required'
        ]);

        if($v->fails()){
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            Barang::create($r->all());
            toastr()->success('Data barang baru berhasil ditambahkan!');
            return redirect(url('admin/barang'));
        }
    }

    public function edit(Barang $barang){
        $no = 1;
        $barangs = Barang::all();
        return view('admin.barang.index', compact('barang', 'barangs', 'no'));
    }

    public function update(Request $r, Barang $barang){
        $v = Validator::make($r->all(),[
            'jenis_barang' => 'required',
            'nama_barang' => 'required'
        ]);

        if($v->fails()){
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            Barang::where('id',$barang->id)
            ->update($r->except(['_token', '_method']));
            toastr()->success('Data barang berhasil diubah!');
            return redirect(url('admin/barang'));
        }
    }

    public function destroy($id){
        Barang::where('id',$id)->delete();
        toastr()->success('Data barang berhasil dihapus!');
        return redirect(url('admin/barang'));
    }
}
