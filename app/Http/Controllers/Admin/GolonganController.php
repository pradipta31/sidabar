<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Golongan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GolonganController extends Controller
{
    public function index(){
        $no = 1;
        $golongans = Golongan::all();
        return view('admin.golongan.index', compact('no','golongans'));
    }

    public function store(Request $r){
        $v = Validator::make($r->all(),[
            'kode' => 'required',
            'nama_golongan' => 'required'
        ]);

        if($v->fails()){
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            Golongan::create($r->all());
            toastr()->success('Data golongan baru berhasil ditambahkan!');
            return redirect(url('admin/golongan'));
        }
    }

    public function edit(Golongan $golongan){
        $no = 1;
        $golongans = Golongan::all();
        return view('admin.golongan.index', compact('golongan', 'golongans', 'no'));
    }

    public function update(Request $r, Golongan $golongan){
        $v = Validator::make($r->all(),[
            'kode' => 'required',
            'nama_golongan' => 'required'
        ]);

        if($v->fails()){
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            Golongan::where('id',$golongan->id)
            ->update($r->except(['_token', '_method']));
            toastr()->success('Data golongan berhasil diubah!');
            return redirect(url('admin/golongan'));
        }
    }

    public function destroy($id){
        Golongan::where('id',$id)->delete();
        toastr()->success('Data Golongan berhasil dihapus!');
        return redirect(url('admin/golongan'));
    }
}
