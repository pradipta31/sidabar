<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Instansi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InstansiController extends Controller
{
    public function index(){
        $no = 1;
        $instansis = Instansi::all();
        return view('admin.instansi.index', compact('no','instansis'));
    }

    public function store(Request $r){
        $v = Validator::make($r->all(),[
            'nama_instansi' => 'required',
            'deskripsi' => 'required'
        ]);

        if($v->fails()){
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            Instansi::create($r->all());
            toastr()->success('Data instansi baru berhasil ditambahkan!');
            return redirect(url('admin/instansi'));
        }
    }

    public function edit(Instansi $instansi){
        $no = 1;
        $instansis = Instansi::all();
        return view('admin.instansi.index', compact('instansi', 'instansis', 'no'));
    }

    public function update(Request $r, Instansi $instansi){
        $v = Validator::make($r->all(),[
            'nama_instansi' => 'required',
            'deskripsi' => 'required'
        ]);

        if($v->fails()){
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            Instansi::where('id',$instansi->id)
            ->update($r->except(['_token', '_method']));
            toastr()->success('Data instansi berhasil diubah!');
            return redirect(url('admin/instansi'));
        }
    }

    public function destroy($id){
        Instansi::where('id',$id)->delete();
        toastr()->success('Data instansi berhasil dihapus!');
        return redirect(url('admin/instansi'));
    }
}
