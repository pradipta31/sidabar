<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Auth;
use App\Penitipan;
use App\Mutasi;
use App\Pengeluaran;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $pengeluarans = Pengeluaran::all();
        return view('admin.keluar.index', compact('no', 'pengeluarans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penitipans = Penitipan::where('status', '!=', 3)->get();
        return view('admin.keluar.create', compact('penitipans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'penitipan_id' => 'required',
            'tgl_pengeluaran' => 'required',
            'alasan' => 'required',
            'file_pengeluaran' => 'required|mimes:pdf'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $id_mutasi = '';
            if ($r->mutasi_id != null) {
                $id_mutasi = $r->mutasi_id;
                $mutasi = Mutasi::where('id', $id_mutasi)->update([
                    'status' => 3
                ]);
            }else{
                $id_mutasi = null;
            }
            // INPUT FILE PDF
            $file = $r->file('file_pengeluaran');
            $file_pengeluaran = time().'.'.$file->getClientOriginalExtension();
            $path_url = 'file/pengeluaran';
            $file->storeAs($path_url,$file_pengeluaran,'public');

            // CREATE MUTASI
            $id_penitipan = $r->penitipan_id;
            $mutasi = Pengeluaran::create([
                'penitipan_id' => $id_penitipan,
                'mutasi_id' => $id_mutasi,
                'user_id' => Auth::user()->id,
                'tgl_pengeluaran' => $r->tgl_pengeluaran,
                'alasan' => $r->alasan,
                'status' => 3,
                'file_pengeluaran' => $file_pengeluaran
            ]);

            $pen = Penitipan::where('id', $id_penitipan)->update([
                'status' => 3
            ]);

            
            toastr()->success('Data telah berhasil dikeluarkan!');
            return redirect(url('admin/pengeluaran'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengeluaran = Pengeluaran::findOrFail($id);
        return view('admin.keluar.detail', compact('pengeluaran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengeluaran = Pengeluaran::findOrFail($id);
        return view('admin.keluar.edit', compact('pengeluaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $validator = Validator::make($r->all(), [
            'penitipan_id' => 'required',
            'tgl_pengeluaran' => 'required',
            'alasan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $pengeluaran = Pengeluaran::where('id', $id)->first();
            if ($r->file('file_pengeluaran')) {
                $f = Validator::make($r->all(), [
                    'file_pengeluaran' => 'required|mimes:pdf'
                ]);
                if ($f->fails()) {
                    toastError($f->messages()->first());
                    return redirect()->back()->withInput();
                } else {
                    $file = $r->file('file_pengeluaran');
                    $file_pengeluaran = time().'.'.$file->getClientOriginalExtension();
                    $path_url = 'file/pengeluaran';
                    unlink(storage_path('app/public/file/pengeluaran/').$pengeluaran->file_pengeluaran);
                    $file->storeAs($path_url,$file_pengeluaran,'public');
                    $pengeluaran->update([
                        'alasan' => $r->alasan,
                        'tgl_pengeluaran' => $r->tgl_pengeluaran,
                        'file_pengeluaran' => $file_pengeluaran
                    ]);
                    toastr()->success('Data Pengeluaran telah berhasil diubah!');
                    return redirect(url('admin/pengeluaran'));
                }
            }else{
                $pengeluaran->update([
                    'alasan' => $r->alasan,
                    'tgl_pengeluaran' => $r->tgl_pengeluaran
                ]);
                toastr()->success('Data Pengeluaran telah berhasil diubah!');
                return redirect(url('admin/pengeluaran'));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengeluaran = Pengeluaran::where('id', $id)->first();
        unlink(storage_path('app/public/file/pengeluaran/').$pengeluaran->file_pengeluaran);
        $pengeluaran->delete();
        $penitipan = Penitipan::where('id', $pengeluaran->penitipan_id)->first();
        $penitipan->update([
            'status' => 2
        ]);
        if($pengeluaran->mutasi_id != null){
            $mutasi = Mutasi::where('id', $pengeluaran->mutasi_id)->update([
                'status' => 2
            ]);
        }
        toastr()->success('Data Pengeluaran telah berhasil dihapus!');
        return redirect(url('admin/pengeluaran'));
    }

    public function downloadFile($get_pengeluaran){
        return response()->download(base_path('storage/app/public/file/pengeluaran/'.$get_pengeluaran));
    }
}
