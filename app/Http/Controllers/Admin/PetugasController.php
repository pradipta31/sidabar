<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PetugasController extends Controller
{
    public function create(){
        return view('admin.petugas.create');
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $users = User::create([
                'nama' => $r->nama,
                'username' => $r->username,
                'email' => $r->email,
                'password' => Hash::make($r->password),
                'no_telp' => $r->no_telp,
                'role' => $r->role,
                'alamat' => $r->alamat,
                'jenis_kelamin' => $r->jenis_kelamin,
                'status' => $r->status
            ]);

            toastr()->success('User baru berhasil ditambahkan!');
            return redirect(url('admin/user'));
        }
    }

    public function index(){
        $no = 1;
        $users = User::all();
        return view('admin.petugas.index', compact('users', 'no'));
    }

    public function edit($id){
        $user = User::where('id',$id)->first();
        return view('admin.petugas.edit', compact('user'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $users = User::where('id',$id)->update([
                'nama' => $r->nama,
                'username' => $r->username,
                'email' => $r->email,
                'no_telp' => $r->no_telp,
                'alamat' => $r->alamat,
                'jenis_kelamin' => $r->jenis_kelamin,
                'status' => $r->status
            ]);

            toastr()->success('Data user berhasil diganti!');
            return redirect(url('admin/user'));
        }
    }

    public function destroy($id){
        $user = User::where('id', $id)->delete();
        toastr()->success('Data user berhasil dihapus!');
        return redirect('admin/user');
    }
}
