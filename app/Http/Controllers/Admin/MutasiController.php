<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;

use App\Pengeluaran;
use App\Penitipan;
use App\Mutasi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MutasiController extends Controller
{
    public function create(){
        $penitipans = Penitipan::where('status', 1)->get();
        return view('admin.mutasi.create', compact('penitipans'));
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'penitipan_id' => 'required',
            'tgl_mutasi' => 'required',
            'keterangan' => 'required',
            'file_mutasi' => 'required|mimes:pdf'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            // INPUT FILE PDF
            $file = $r->file('file_mutasi');
            $file_mutasi = time().'.'.$file->getClientOriginalExtension();
            $path_url = 'file/mutasi';
            $file->storeAs($path_url,$file_mutasi,'public');

            // CREATE MUTASI
            $id = $r->penitipan_id;
            $mutasi = Mutasi::create([
                'penitipan_id' => $id,
                'user_id' => Auth::user()->id,
                'tgl_mutasi' => $r->tgl_mutasi,
                'keterangan' => $r->keterangan,
                'status' => 2,
                'file_mutasi' => $file_mutasi
            ]);

            $pen = Penitipan::where('id', $id)->update([
                'status' => 2
            ]);
            toastr()->success('Data telah berhasil termutasi!');
            return redirect(url('admin/mutasi'));
        }
    }

    public function index(){
        $no = 1;
        $mutasis = Mutasi::all();
        return view('admin.mutasi.index', compact('mutasis', 'no'));
    }

    public function detail($id){
        $mutasi = Mutasi::findOrFail($id);
        return view('admin.mutasi.detail', compact('mutasi'));
    }

    public function downloadFile($get_mutasi){
        return response()->download(base_path('storage/app/public/file/mutasi/'.$get_mutasi));
    }

    public function edit($id){
        $mutasi = Mutasi::findOrFail($id);
        return view('admin.mutasi.edit', compact('mutasi'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'penitipan_id' => 'required',
            'tgl_mutasi' => 'required',
            'keterangan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $mutasi = Mutasi::where('id', $id)->first();
            if ($r->file('file_mutasi')) {
                $f = Validator::make($r->all(), [
                    'file_mutasi' => 'required|mimes:pdf'
                ]);
                if ($f->fails()) {
                    toastError($f->messages()->first());
                    return redirect()->back()->withInput();
                } else {
                    $file = $r->file('file_mutasi');
                    $file_mutasi = time().'.'.$file->getClientOriginalExtension();
                    $path_url = 'file/mutasi';
                    unlink(storage_path('app/public/file/mutasi/').$mutasi->file_mutasi);
                    $file->storeAs($path_url,$file_mutasi,'public');
                    $mutasi->update([
                        'keterangan' => $r->keterangan,
                        'tgl_mutasi' => $r->tgl_mutasi,
                        'file_mutasi' => $file_mutasi
                    ]);
                    toastr()->success('Data Mutasi telah berhasil diubah!');
                    return redirect(url('admin/mutasi'));
                }
            }else{
                $mutasi->update([
                    'keterangan' => $r->keterangan,
                    'tgl_mutasi' => $r->tgl_mutasi
                ]);
                toastr()->success('Data Mutasi telah berhasil diubah!');
                return redirect(url('admin/mutasi'));
            }
        }
    }

    public function keluarMutasi($id){
        $mutasi = Mutasi::findOrFail($id);
        return view('admin.mutasi.pengeluaran', compact('mutasi'));
    }

    public function destroy($id){
        $mutasi = Mutasi::where('id', $id)->first();
        unlink(storage_path('app/public/file/mutasi/').$mutasi->file_mutasi);
        $mutasi->delete();
        $pengeluaran = Pengeluaran::where('mutasi_id', $id)->first();
        if ($pengeluaran != NULL) {
            unlink(storage_path('app/public/file/pengeluaran/').$mutasi->file_pengeluaran);
            $pengeluaran->delete();
        }
        Penitipan::where('id', $mutasi->penitipan_id)->update(['status' => 1]);
        toastr()->success('Data Mutasi telah hapus!');
        return redirect(url('admin/mutasi'));
    }
}
