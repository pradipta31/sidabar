<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';

    protected $fillable = [
        'jenis_barang',
        'nama_barang'
    ];

    public function penitipan(){
        return $this->hasOne('app\Penitipan');
    }
}
