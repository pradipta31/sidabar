<?php

use Illuminate\Database\Seeder;

class GolonganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('golongans')->insert([
            [
                'kode' => 'RBS1',
                'nama_golongan' => 'Benda Sitaan Penyidikan',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'kode' => 'RBR1',
                'nama_golongan' => 'Benda Rampasan Penyidikan',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        ]);
    }
}
