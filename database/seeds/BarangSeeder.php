<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barangs')->insert([
            [
                'jenis_barang' => 'Di Luar RUPBASAN',
                'nama_barang' => 'Honda Vario DK 1234 AA',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'jenis_barang' => 'Di Luar RUPBASAN',
                'nama_barang' => 'Honda Supra DK 4321 BB',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'jenis_barang' => 'Di Luar RUPBASAN',
                'nama_barang' => 'Yamaha Mio DK 5423 CC',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'jenis_barang' => 'Di Dalam RUPBASAN',
                'nama_barang' => 'Terios TX DK 1432 DD',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'jenis_barang' => 'Di Dalam RUPBASAN',
                'nama_barang' => 'Avanza Veloz DK 5544 EE',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'jenis_barang' => 'Di Dalam RUPBASAN',
                'nama_barang' => 'Avanza G DK 2211 FF',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
        ]);
    }
}
