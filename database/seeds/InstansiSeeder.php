<?php

use Illuminate\Database\Seeder;

class InstansiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instansis')->insert([
            [
                'nama_instansi' => 'Polda Bali',
                'deskripsi' => 'Kepolisian Daerah Bali',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama_instansi' => 'Kejari Denpasar',
                'deskripsi' => 'Kejaksaan Negeri Denpasar',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama_instansi' => 'Kejati',
                'deskripsi' => 'Kejaksaan Tinggi Bali',
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
        ]);
    }
}
