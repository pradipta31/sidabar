<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('123'),
                'role' => 'admin',
                'no_telp' => '081222333444',
                'alamat' => 'Jl. Raya I Gusti Ngurah Rai, No. XXX',
                'jenis_kelamin' => 'Pria',
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama' => 'Petugas',
                'username' => 'petugas',
                'email' => 'petugas@gmail.com',
                'password' => Hash::make('123'),
                'role' => 'petugas',
                'no_telp' => '0819998887788',
                'alamat' => 'Jl. Raya I Gusti Ngurah Rai, No. 999',
                'jenis_kelamin' => 'Pria',
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
        ]);
    }
}
