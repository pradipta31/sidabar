<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/', function(){
    return redirect('/home');
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');

    // ROUTE ADMIN
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function(){
        // ROUTE LAPORAN
        Route::get('laporan', function(){
            return view('admin.laporan.index');
        });
        // ROUTE BARANG
        Route::get('barang', 'BarangController@index')->name('create:barang');
        Route::post('barang', 'BarangController@store')->name('store:barang');
        Route::get('barang/edit/{barang}', 'BarangController@edit')->name('edit:barang');
        Route::put('barang/update/{barang}', 'BarangController@update')->name('update:barang');
        Route::delete('barang/delete/{id}', 'BarangController@destroy');

        // ROUTE INSTANSI
        Route::get('instansi', 'InstansiController@index')->name('create:instansi');
        Route::post('instansi', 'InstansiController@store')->name('store:instansi');
        Route::get('instansi/edit/{instansi}', 'InstansiController@edit')->name('edit:instansi');
        Route::put('instansi/update/{instansi}', 'InstansiController@update')->name('update:instansi');
        Route::delete('instansi/delete/{id}', 'InstansiController@destroy');

        // ROUTE GOLONGAN
        Route::get('golongan', 'GolonganController@index')->name('create:golongan');
        Route::post('golongan', 'GolonganController@store')->name('store:golongan');
        Route::get('golongan/edit/{golongan}', 'GolonganController@edit')->name('edit:golongan');
        Route::put('golongan/update/{golongan}', 'GolonganController@update')->name('update:golongan');
        Route::delete('golongan/delete/{id}', 'GolonganController@destroy');

        // ROUTE KELOLA PETUGAS
        Route::get('user/tambah', 'PetugasController@create');
        Route::post('user/tambah', 'PetugasController@store');
        Route::get('user', 'PetugasController@index');
        Route::get('user/{id}/edit', 'PetugasController@edit');
        Route::put('user/{id}/edit', 'PetugasController@update');
        Route::delete('user/delete/{id}', 'PetugasController@destroy');

        // ROUTE KELOLA PENITIPAN
        Route::get('penitipan/tambah', 'PenitipanController@create');
        Route::post('penitipan/tambah', 'PenitipanController@store');
        Route::get('penitipan', 'PenitipanController@index');
        Route::get('penitipan/{id}/edit', 'PenitipanController@edit');
        Route::put('penitipan/{id}/edit', 'PenitipanController@update');
        Route::get('penitipan/detail/{id}', 'PenitipanController@detail');
        Route::get('penitipan/{get_penitipan}/download', 'PenitipanController@downloadFile')->name('penitipan.download');
        Route::get('penitipan/mutasi/{id}', 'PenitipanController@mutasiPenitipan');
        Route::get('penitipan/keluar/{id}', 'PenitipanController@keluarPenitipan');
        Route::delete('penitipan/delete/{id}', 'PenitipanController@destroy');
        Route::get('penitipan/cetak', 'PenitipanController@cetak');

        // ROUTE KELOLA MUTASI
        Route::get('mutasi/tambah', 'MutasiController@create');
        Route::post('mutasi/tambah', 'MutasiController@store');
        Route::get('mutasi', 'MutasiController@index');
        Route::get('mutasi/detail/{id}', 'MutasiController@detail');
        Route::get('mutasi/{get_mutasi}/download', 'MutasiController@downloadFile')->name('mutasi.download');
        Route::get('mutasi/{id}/edit', 'MutasiController@edit');
        Route::put('mutasi/{id}/edit', 'MutasiController@update');
        Route::get('mutasi/keluar/{id}', 'MutasiController@keluarMutasi');
        Route::delete('mutasi/delete/{id}', 'MutasiController@destroy');
        Route::get('mutasi/cetak', 'MutasiController@cetak');

        // ROUTE KELOLA PENGELUARAN
        Route::get('pengeluaran/tambah', 'PengeluaranController@create');
        Route::post('pengeluaran/tambah', 'PengeluaranController@store');
        Route::get('pengeluaran', 'PengeluaranController@index');
        Route::get('pengeluaran/{id}/edit', 'PengeluaranController@edit');
        Route::put('pengeluaran/{id}/edit', 'PengeluaranController@update');
        Route::get('pengeluaran/detail/{id}', 'PengeluaranController@show');
        Route::get('pengeluaran/{get_pengeluaran}/download', 'PengeluaranController@downloadFile')->name('pengeluaran.download');
        Route::delete('pengeluaran/delete/{id}', 'PengeluaranController@destroy');
    });

    Route::group(['namespace' => 'Petugas', 'prefix' => 'petugas'], function(){
        Route::get('laporan', function(){
            return view('admin.laporan.index');
        });
        
        // ROUTE BARANG
        Route::get('barang', 'BarangController@index')->name('create:barang');
        Route::post('barang', 'BarangController@store')->name('store:barang');
        Route::get('barang/edit/{barang}', 'BarangController@edit')->name('edit:barang');
        Route::put('barang/update/{barang}', 'BarangController@update')->name('update:barang');
        Route::delete('barang/delete/{id}', 'BarangController@destroy');

        // ROUTE KELOLA PENITIPAN
        Route::get('penitipan/tambah', 'PenitipanController@create');
        Route::post('penitipan/tambah', 'PenitipanController@store');
        Route::get('penitipan', 'PenitipanController@index');
        Route::get('penitipan/{id}/edit', 'PenitipanController@edit');
        Route::put('penitipan/{id}/edit', 'PenitipanController@update');
        Route::get('penitipan/detail/{id}', 'PenitipanController@detail');
        Route::get('penitipan/{get_penitipan}/download', 'PenitipanController@downloadFile')->name('penitipan.download');
        Route::get('penitipan/mutasi/{id}', 'PenitipanController@mutasiPenitipan');
        Route::get('penitipan/keluar/{id}', 'PenitipanController@keluarPenitipan');
        Route::delete('penitipan/delete/{id}', 'PenitipanController@destroy');
        Route::get('penitipan/cetak', 'PenitipanController@cetak');

        // ROUTE KELOLA MUTASI
        Route::get('mutasi/tambah', 'MutasiController@create');
        Route::post('mutasi/tambah', 'MutasiController@store');
        Route::get('mutasi', 'MutasiController@index');
        Route::get('mutasi/detail/{id}', 'MutasiController@detail');
        Route::get('mutasi/{get_mutasi}/download', 'MutasiController@downloadFile')->name('mutasi.download');
        Route::get('mutasi/{id}/edit', 'MutasiController@edit');
        Route::put('mutasi/{id}/edit', 'MutasiController@update');
        Route::get('mutasi/keluar/{id}', 'MutasiController@keluarMutasi');
        Route::delete('mutasi/delete/{id}', 'MutasiController@destroy');

        // ROUTE KELOLA PENGELUARAN
        Route::get('pengeluaran/tambah', 'PengeluaranController@create');
        Route::post('pengeluaran/tambah', 'PengeluaranController@store');
        Route::get('pengeluaran', 'PengeluaranController@index');
        Route::get('pengeluaran/{id}/edit', 'PengeluaranController@edit');
        Route::put('pengeluaran/{id}/edit', 'PengeluaranController@update');
        Route::get('pengeluaran/detail/{id}', 'PengeluaranController@show');
        Route::get('pengeluaran/{get_pengeluaran}/download', 'PengeluaranController@downloadFile')->name('pengeluaran.download');
    });

    // ROUTE PETUGAS
});
