@extends('layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('breadcrumb', 'Dashboard')
@section('detail_breadcrumb', 'Control Panel')
@section('content')
    @include('layouts.breadcrumb')
    @php
        $role = '';
        if(Auth::user()->role == 'admin'){
            $role = 'admin';
        }else{
            $role = 'petugas';
        }
        
    @endphp
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/barang')}}">
                    <div class="small-box bg-navy">
                        <div class="inner">
                            <h3>{{$barang}}</h3>
            
                            <p>Jumlah Barang</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-clone"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                @if (Auth::user()->role == 'admin')
                    <a href="{{url($role.'/instansi')}}">    
                @else
                    <a href="#">
                @endif
                    <div class="small-box bg-olive">
                        <div class="inner">
                            <h3>{{$instansi}}</h3>
            
                            <p>Jumlah Instansi</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-building"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                @if (Auth::user()->role == 'admin')
                    <a href="{{url($role.'/golongan')}}">    
                @else
                    <a href="#">
                @endif
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{$golongan}}</h3>
            
                            <p>Jumlah Golongan</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/penitipan')}}">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>{{$penitipan}}</h3>
            
                            <p>Jumlah Penitipan</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-hourglass-half"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/mutasi')}}">
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3>{{$mutasi}}</h3>
            
                            <p>Jumlah Mutasi</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-exchange"></i>
                        </div>
                    
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/pengeluaran')}}">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$pengeluaran}}</h3>
            
                            <p>Jumlah Barang Keluar</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-trash"></i>
                        </div>
                    
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <h4 class="header-title" align="center">Statistik Penitipan {{date('Y')}}</h4>
                <canvas id="chartPenitipan" class="chartjs" width="undefined" height="undefined"></canvas>
            </div>
            <div class="col-sm-4">
                <h4 class="header-title" align="center">Statistik Mutasi {{date('Y')}}</h4>
                <canvas id="chartMutasi" class="chartjs" width="undefined" height="undefined"></canvas>
            </div>
            <div class="col-sm-4">
                <h4 class="header-title" align="center">Statistik Pengeluaran {{date('Y')}}</h4>
                <canvas id="chartPengeluaran" class="chartjs" width="undefined" height="undefined"></canvas>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/chart/Chart.js')}}"></script>
    <script type="text/javascript">
        var ctx = document.getElementById("chartPenitipan").getContext('2d');
        var chartPenitipan = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($label); ?>,
                datasets: [{
                    label: 'Data Penitipan',
                    backgroundColor: '#ADD8E6',
                    borderColor: '#93C3D2',
                    data: <?php echo json_encode($jumlahPen); ?>
                }],
                options: {
                    animation: {
                        onProgress: function(animation) {
                            progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                        }
                    }
                }
            },
        });

        var ctx = document.getElementById("chartMutasi").getContext('2d');
        var chartMutasi = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($label); ?>,
                datasets: [{
                    label: 'Data Mutasi',
                    backgroundColor: '#d14949',
                    borderColor: '#cf463c',
                    data: <?php echo json_encode($jumlahMut); ?>
                }],
                options: {
                    animation: {
                        onProgress: function(animation) {
                            progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                        }
                    }
                }
            },
        });

        var ctx = document.getElementById("chartPengeluaran").getContext('2d');
        var chartPengeluaran = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($label); ?>,
                datasets: [{
                    label: 'Data Pengeluaran',
                    backgroundColor: '#7ecca2',
                    borderColor: '#93d2b0',
                    data: <?php echo json_encode($jumlahPeng); ?>
                }],
                options: {
                    animation: {
                        onProgress: function(animation) {
                            progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                        }
                    }
                }
            },
        });
    </script>
    
@endsection
