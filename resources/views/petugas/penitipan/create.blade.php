@extends('layouts.master',['activeMenu' => 'penitipan'])
@section('title','Tambah Penitipan')
@section('breadcrumb', 'Tambah Penitipan')
@section('detail_breadcrumb', 'Tambah Penitipan Baru')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    
    <section class="content">
        <form class="" action="{{url('petugas/penitipan/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Penitipan Baru</h3>
                            <p><small>Tambahkan data penitipan barang baru.</small></p>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Golongan </label>
                                <select name="golongan_id" class="form-control selectGolongan" value="{{old('golongan_id')}}">
                                    <option value="">-- Pilih Golongan --</option>
                                    @foreach ($golongans as $golongan)
                                        <option value="{{$golongan->id}}">{{$golongan->kode}} - {{$golongan->nama_golongan}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Barang </label>
                                <select name="barang_id" class="form-control selectBarang" value="{{old('barang_id')}}">
                                    <option value="">-- Pilih Barang --</option>
                                    @foreach ($barangs as $barang)
                                        <option value="{{$barang->id}}">{{$barang->jenis_barang}} - {{$barang->nama_barang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Penitipan</label>
                                <input type="date" name="tgl_penitipan" class="form-control" value="{{old('tgl_penitipan')}}" placeholder="Masukkan Tanggal Penitipan">
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah</label>
                                <input type="number" name="jumlah" class="form-control" value="{{old('jumlah')}}" placeholder="Masukkan Jumlah Penitipan">
                            </div>
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control">
                                <small>NB: File gambar harus format JPEG/JPG/PNG</small>
                            </div>
                            <div class="form-group">
                                <label for="">Instansi Penitip</label>
                                <select name="instansi_id" class="form-control selectInstansi" value="{{old('instansi_id')}}">
                                    <option value="">-- Pilih Instansi --</option>
                                    @foreach ($instansis as $instansi)
                                        <option value="{{$instansi->id}}">{{$instansi->nama_instansi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Kasus</label>
                                <hr>
                                <textarea name="kasus" id="" cols="30" rows="10" class="form-control">{{old('kasus')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Penitipan</label>
                                <input type="file" class="form-control" name="file_penitipan">
                                <small>NB: File Penitipan harus format PDF.</small>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    }); 
    
    $(function () {
        //Initialize Select2 Elements
        $('.selectGolongan').select2()
        $('.selectBarang').select2()
        $('.selectInstansi').select2()
        
        // var e = document.getElementById("gol_id");
        // var tes = e.options[e.selectedIndex].text;
        // console.log(tes);

        // var select = document.getElementById('gol_id');
        // var text = document.getElementById('text');
        // select.onchange = function(){
        //     text.value = select.options[select.selectedIndex].text;
        // }
    });
</script>

@endsection