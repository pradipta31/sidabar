@extends('layouts.master',['activeMenu' => 'penitipan'])
@section('title','Data Penitipan')
@section('breadcrumb', 'Data Penitipan')
@section('detail_breadcrumb', 'Manajemen Data Penitipan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/penitipan/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Penitipan
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tablePenitipan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Petugas</th>
                                        <th>Kode Register</th>
                                        <th>Tanggal Penitipan</th>
                                        <th>Nama Barang</th>
                                        <th>Instansi Penitip</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($penitipans as $penitipan)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$penitipan->user->nama}}</td>
                                            <td>{{$penitipan->kode_register}}</td>
                                            <td>{{date('d F Y', strtotime($penitipan->tgl_penitipan))}}</td>
                                            <td>{{$penitipan->barang->nama_barang}}</td>
                                            <td>{{$penitipan->instansi->nama_instansi}}</td>
                                            <td>
                                                @if ($penitipan->status == 1)
                                                    <span class="label label-primary">Penitipan</span>
                                                @elseif($penitipan->status == 2)
                                                    <span class="label label-info">Termutasi</span>
                                                @elseif($penitipan->status == 3)
                                                    <span class="label label-danger">Dikeluarkan</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('petugas/penitipan/detail/'.$penitipan->id)}}" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-eye"></i>
                                                    Detail
                                                </a>
                                                <a href="{{url('petugas/penitipan/'.$penitipan->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deletePenitipan({{ $penitipan->id }})">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tablePenitipan').dataTable()
        });

        function deletePenitipan(id){
            swal({
                title: "Anda yakin?",
                text: "Data penitipan akan terhapus secara permanen! Jika ada data yang berhubungan dengan data penitipan seperti mutasi dan pengeluaran akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Penitipan yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('petugas/penitipan/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
