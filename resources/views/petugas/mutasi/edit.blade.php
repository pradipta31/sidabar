@extends('layouts.master',['activeMenu' => 'mutasi'])
@section('title','Edit Mutasi')
@section('breadcrumb', 'Edit Mutasi')
@section('detail_breadcrumb', 'Edit Mutasi Penitipan Barang')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <style>
        .select2{
            height: 50%;
        }
    </style>
    <section class="content">
        <form class="" action="{{url('petugas/mutasi/'.$mutasi->id.'/edit')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Mutasi {{$mutasi->penitipan->nama_barang}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Barang</label>
                                <select name="penitipan_id" class="form-control select2" value="{{$mutasi->penitipan_id}}">
                                    <option value="{{$mutasi->id}}">#{{$mutasi->penitipan->kode_register}} - {{$mutasi->penitipan->barang->nama_barang}}</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Tanggal Mutasi</label>
                                <input type="date" name="tgl_mutasi" class="form-control" value="{{$mutasi->tgl_mutasi}}">
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan Pemindahan</label>
                                <textarea name="keterangan" cols="30" rows="3" class="form-control">{{$mutasi->keterangan}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Mutasi</label>
                                <input type="file" name="file_mutasi" class="form-control">
                                <small>NB: Kosongkan jika tidak ingin mengubah file mutasi (Format: PDF)</small>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveThis(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>
@endsection