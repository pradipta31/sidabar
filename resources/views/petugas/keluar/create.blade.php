@extends('layouts.master',['activeMenu' => 'pengeluaran'])
@section('title','Tambah Barang Keluar')
@section('breadcrumb', 'Tambah Barang Keluar')
@section('detail_breadcrumb', 'Tambah Barang Keluar Penitipan Barang')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <style>
        .select2{
            height: 50%;
        }
    </style>
    <section class="content">
        <form class="" action="{{url('petugas/pengeluaran/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Barang Keluar Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Barang</label>
                                <select name="penitipan_id" class="form-control select2" value="{{old('penitipan_id')}}">
                                    <option value="">Pilih Barang</option>
                                    @foreach ($penitipans as $penitipan)
                                        @php 
                                            $status = '';
                                            if ($penitipan->status == 1) {
                                                $status = 'PENITIPAN';
                                            } else {
                                                $status = 'MUTASI';
                                            }
                                            
                                        @endphp
                                        <option value="{{$penitipan->id}}" {{$penitipan->id == $penitipan->id ? 'selected' : ''}}>
                                            {{$status}} - #{{$penitipan->kode_barang}} - {{$penitipan->nama_barang}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Tanggal Mutasi</label>
                                <input type="date" name="tgl_pengeluaran" class="form-control" value="{{old('tgl_pengeluaran')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Alasan Pengeluaran</label>
                                <textarea name="alasan" cols="30" rows="3" class="form-control">{{old('alasan')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Pengeluaran</label>
                                <input type="file" name="file_pengeluaran" class="form-control">
                                <small>NB: Upload file pengeluaran format *PDF</small>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveThis(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>
@endsection