@extends('layouts.master',['activeMenu' => 'user'])
@section('title','Tambah User')
@section('breadcrumb', 'Tambah User')
@section('detail_breadcrumb', 'Tambah User Baru')
@section('content')
    @include('layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form class="" action="{{url('admin/user/'.$user->id.'/edit')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <input type="hidden" name="length" value="6">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit User: {{$user->nama}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{$user->nama}}" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control" value="{{$user->username}}" placeholder="Masukkan Username">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{$user->email}}" placeholder="Masukkan Email">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label class="col-form-label">Password</label>
                                        <input type="text" class="form-control" name="password" value="{{old('password')}}">
                                        <small>NB: Kosongkan jika tidak ingin mengganti password.</small>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-form-label"></label>
                                        <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                            <i class="fa fa-refresh"></i>
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label for="">Role</label>
                                <select name="role" class="form-control" value="{{$user->role}}">
                                    <option value="">Pilih Role User</option>
                                    <option value="admin" {{$user->role == 'admin' ? 'selected' : ''}}>Admin</option>
                                    <option value="petugas" {{$user->role == 'petugas' ? 'selected' : ''}}>Petugas</option>
                                </select>
                            </div> --}}
                            <div class="form-group">
                                <label for="">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control" value="{{$user->jenis_kelamin}}">
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="Pria" {{$user->jenis_kelamin == 'Pria' ? 'selected' : ''}}>Pria</option>
                                    <option value="Wanita" {{$user->jenis_kelamin == 'Wanita' ? 'selected' : ''}}>Wanita</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">No Telepon</label>
                                <input type="number" name="no_telp" class="form-control" value="{{$user->no_telp}}" placeholder="Masukkan Nomor Telepon">
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <textarea name="alamat" cols="30" rows="3" class="form-control">{{$user->alamat}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{$user->status}}">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{$user->status == 1 ? 'selected' : ''}}>Aktif</option>
                                    <option value="0" {{$user->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                                </select>
                                <small>Note: Status aktif dapat melakukan login.</small>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                <a href="{{url('admin/user')}}" class="btn btn-default">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function saveThis(r){
            swal({
                title: "User baru akan ditambahkan",
                text: "User akan menerima email dan harus melakukan konfirmasi untuk dapat melakukan login.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((result) => {
                if (result) {
                    swal("Email berhasil terkirim!", {
                        icon: "success",
                    });
                }
            });
        }
    </script>
@endsection