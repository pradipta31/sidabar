@extends('layouts.master',['activeMenu' => 'mutasi'])
@section('title','Data Mutasi')
@section('breadcrumb', 'Data Mutasi')
@section('detail_breadcrumb', 'Manajemen Data Mutasi')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/mutasi/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Mutasi
                        </a> --}}
                        {{-- <div class="row" style="margin-bottom: 10px">
                            <form action="{{url('admin/mutasi/cetak')}}" method="GET">
                                <div class="col-md-2">
                                    <input type="date" name="date_1" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <input type="date" name="date_2" class="form-control">
                                </div>
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-download"></i>
                                        Export Data Mutasi
                                    </button>
                                </div>
                            </form>
                        </div> --}}
                        <div class="table-responsive">
                            <table id="tablePenitipan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Petugas</th>
                                        <th>Kode Barang</th>
                                        <th>Tanggal Mutasi</th>
                                        <th>Nama Barang</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mutasis as $mutasi)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$mutasi->user->nama}}</td>
                                            <td>{{$mutasi->penitipan->kode_register}}</td>
                                            <td>{{$mutasi->tgl_mutasi}}</td>
                                            <td>{{$mutasi->penitipan->barang->nama_barang}}</td>
                                            <td>
                                                @if ($mutasi->status == 1)
                                                    <span class="label label-primary">Penitipan</span>
                                                @elseif($mutasi->status == 2)
                                                    <span class="label label-info">Termutasi</span>
                                                @elseif($mutasi->status == 3)
                                                    <span class="label label-danger">Dikeluarkan</span>
                                                @endif
                                            </td>
                                                
                                            <td>
                                                <a href="{{url('admin/mutasi/detail/'.$mutasi->id)}}" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-eye"></i>
                                                    Detail
                                                </a>
                                                <a href="{{url('admin/mutasi/'.$mutasi->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteMutasi({{ $mutasi->id }})">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tablePenitipan').dataTable()
        });

        function deleteMutasi(id){
            swal({
                title: "Anda yakin?",
                text: "Data mutasi akan terhapus secara permanen! Data yang berhubungan dengan mutasi seperti pengeluaran juga akan ikut terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data mutasi yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/mutasi/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
