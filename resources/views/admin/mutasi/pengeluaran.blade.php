@extends('layouts.master',['activeMenu' => 'pengeluaran'])
@section('title','Tambah Barang Keluar')
@section('breadcrumb', 'Tambah Barang Keluar')
@section('detail_breadcrumb', 'Tambah Barang Keluar Penitipan Barang')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <style>
        .select2{
            height: 50%;
        }
    </style>
    <section class="content">
        <form class="" action="{{url('admin/pengeluaran/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="penitipan_id" value="{{$mutasi->penitipan_id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Keluarkan Barang: {{$mutasi->penitipan->nama_barang}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Barang</label>
                                <select name="mutasi_id" class="form-control select2" value="{{old('mutasi_id')}}">
                                    <option value="{{$mutasi->id}}">
                                        #{{$mutasi->penitipan->kode_register}} - {{$mutasi->penitipan->barang->nama_barang}}
                                    </option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Tanggal Pengeluaran</label>
                                <input type="date" name="tgl_pengeluaran" class="form-control" value="{{old('tgl_pengeluaran')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Alasan Pengeluaran</label>
                                <textarea name="alasan" cols="30" rows="3" class="form-control">{{old('alasan')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Pengeluaran</label>
                                <input type="file" name="file_pengeluaran" class="form-control">
                                <small>NB: Upload file Pengeluaran format *PDF</small>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveThis(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>
@endsection