@extends('layouts.master',['activeMenu' => 'mutasi'])
@section('title','Detail Penitipan dan Mutasi')
@section('breadcrumb', 'Detail Penitipan dan Mutasi')
@section('detail_breadcrumb', 'Detail Penitipan dan Mutasi '.$mutasi->penitipan->barang->nama_barang)
@section('content')
    @include('layouts.breadcrumb')
    <div class="pad margin no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Detail Informasi Penitipan dan mutasi</h4>
            Dalam menu ini berisi detail mengenai informasi penitipan dan mutasi barang yang terpilih.
        </div>
    </div>

    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                <i class="fa fa-archive"></i> {{$mutasi->penitipan->barang->nama_barang}} - #{{$mutasi->penitipan->kode_register}}
                <small class="pull-right">Tanggal Penitipan: {{date('d F Y', strtotime($mutasi->penitipan->tgl_penitipan))}}</small>
                </h2>
            </div>
        </div>
        {{-- <div class="row invoice-info">
            <div class="col-sm-12 invoice-col">
                <h3>
                    Petugas :
                    <strong>{{$mutasi->penitipan->user->nama}}</strong><br>
                </h3>
            </div>
        </div> --}}

        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Tgl Mutasi</th>
                        <th>Jumlah</th>
                        <th>Gambar</th>
                        <th>Instansi Penitip</th>
                        <th>Golongan</th>
                        <th>File Penitipan</th>
                        <th>File Mutasi</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{date('d F Y', strtotime($mutasi->tgl_mutasi))}}</td>
                            <td>{{$mutasi->penitipan->jumlah}}</td>
                            <td>
                                @if ($mutasi->penitipan->gambar != null)
                                    <span class="label label-primary">Ada</span>
                                @else
                                    <span class="label label-warning">Tidak Ada</span>
                                @endif
                            </td>
                            <td>{{$mutasi->penitipan->instansi->nama_instansi}}</td>
                            <td>{{$mutasi->penitipan->golongan->kode}} - {{$mutasi->penitipan->golongan->nama_golongan}}</td>
                            <td>
                                @if ($mutasi->penitipan->file_penitipan != null)
                                    <span class="label label-primary">Ada</span>
                                @else
                                    <span class="label label-warning">Tidak Ada</span>
                                @endif
                            </td>
                            <td>
                                @if ($mutasi->file_mutasi != null)
                                    <span class="label label-primary">Ada</span>
                                @else
                                    <span class="label label-warning">Tidak Ada</span>
                                @endif
                            </td>
                            <td>
                                @if ($mutasi->penitipan->status == 1)
                                    <span class="label label-primary">Penitipan</span>
                                @elseif($mutasi->penitipan->status == 2)
                                    <span class="label label-info">Termutasi</span>
                                @elseif($mutasi->penitipan->status == 3)
                                    <span class="label label-danger">Dikeluarkan</span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <p class="lead">Keterangan Mutasi:</p>
                
                <div class="box box-solid">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    {{$mutasi->penitipan->nama_barang}}
                                </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="box-body" style="overflow-x: auto">
                                {{$mutasi->keterangan}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row no-print">
            <div class="col-xs-12">
                {{-- <a href="{{ route('penitipan.download', $mutasi->penitipan->file_penitipan) }}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print File Penitipan</a> --}}
                <button class="btn btn-primary" onClick="showImage('{{$mutasi->penitipan->gambar}}');"><i class="fa fa-image"></i> Lihat Gambar</button>
                <a href="{{ route('mutasi.download', $mutasi->file_mutasi) }}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print File Mutasi</a>
                @if ($mutasi->status == 2)
                    <a href="{{url('admin/mutasi/keluar/'.$mutasi->id)}}" class="btn btn-danger pull-right">
                        <i class="fa fa-trash"></i> Keluarkan Barang
                    </a>
                @else
                    <a href="#" class="btn btn-danger pull-right" disabled>
                        <i class="fa fa-trash"></i> Keluarkan Barang
                    </a>
                @endif
                {{-- <a href="{{url('admin/penitipan/mutasi/'.$mutasi->penitipan->id)}}" class="btn btn-success pull-right" style="margin-right: 5px;"><i class="fa fa-exchange"></i> Mutasi Barang
                </a> --}}
                
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script>
        function showImage(gambar){
            bootbox.dialog({
                message: '<img src="{{asset('images/penitipan')}}/'+gambar+'" class="img-responsive">',
                closeButton: true,
                size: 'medium'
            });
        }
    </script>
@endsection