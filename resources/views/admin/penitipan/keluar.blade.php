@extends('layouts.master',['activeMenu' => 'keluar'])
@section('title','Tambah Pengeluaran Barang')
@section('breadcrumb', 'Tambah Pengeluaran Barang')
@section('detail_breadcrumb', 'Tambah Pengeluaran Penitipan Barang')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('admin/pengeluaran/tambah')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Barang Keluar Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Barang</label>
                                <select name="penitipan_id" class="form-control select2" value="{{old('penitipan_id')}}">
                                    <option value="{{$pen->id}}">{{$pen->kode_register}} - {{$pen->barang->nama_barang}}</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Tanggal Keluar</label>
                                <input type="date" name="tgl_pengeluaran" class="form-control" value="{{old('tgl_pengeluaran')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Alasan Pengeluaran</label>
                                <textarea name="alasan" cols="30" rows="3" class="form-control">{{old('alasan')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Pengeluaran</label>
                                <input type="file" name="file_pengeluaran" class="form-control">
                                <small>NB: Upload file pengeluaran format *PDF</small>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveThis(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>
@endsection