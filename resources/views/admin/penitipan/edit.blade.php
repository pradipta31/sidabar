@extends('layouts.master',['activeMenu' => 'penitipan'])
@section('title','Edit Penitipan')
@section('breadcrumb', 'Edit Penitipan')
@section('detail_breadcrumb', 'Edit Data Penitipan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    
    <section class="content">
        <form class="" action="{{url('admin/penitipan/'.$penitipan->id.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Penitipan Barang - #{{$penitipan->kode_register}}</h3>
                            <p><small>Edit Data penitipan barang.</small></p>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Golongan </label>
                                <select name="golongan_id" class="form-control selectGolongan" value="{{$penitipan->golongan_id}}" disabled>
                                    <option value="">-- Pilih Golongan --</option>
                                    @foreach ($golongans as $golongan)
                                        <option value="{{$golongan->id}}" {{$penitipan->golongan_id == $golongan->id ? 'selected' : ''}}>{{$golongan->kode}} - {{$golongan->nama_golongan}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Barang </label>
                                <select name="barang_id" class="form-control selectBarang" value="{{$penitipan->barang_id}}">
                                    <option value="">-- Pilih Barang --</option>
                                    @foreach ($barangs as $barang)
                                        <option value="{{$barang->id}}" {{$penitipan->barang_id == $barang->id ? 'selected' : ''}}>{{$barang->jenis_barang}} - {{$barang->nama_barang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Penitipan</label>
                                <input type="date" name="tgl_penitipan" class="form-control" value="{{$penitipan->tgl_penitipan}}" placeholder="Masukkan Tanggal Penitipan">
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah</label>
                                <input type="number" name="jumlah" class="form-control" value="{{$penitipan->jumlah}}" placeholder="Masukkan Jumlah Penitipan">
                            </div>
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control">
                                <small>Upload dokumen baru untuk mengubah dokumen sebelumnya. (*Format: JPEG/JPG/PNG)</small>
                            </div>
                            <div class="form-group">
                                <label for="">Instansi Penitip</label>
                                <select name="instansi_id" class="form-control selectInstansi" value="{{$penitipan->instansi_id}}">
                                    <option value="">-- Pilih Instansi --</option>
                                    @foreach ($instansis as $instansi)
                                        <option value="{{$instansi->id}}" {{$penitipan->instansi_id == $instansi->id ? 'selected' : ''}}>{{$instansi->nama_instansi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Masukkan Kasus</label>
                                <p><small>Masukkan kasus dari barang tersebut secara mendetail.</small></p>
                                <hr>
                                <textarea name="kasus" id="" cols="30" rows="10" class="form-control">{{$penitipan->kasus}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Penitipan</label>
                                <input type="file" class="form-control" name="file_penitipan">
                                <small>Upload dokumen baru untuk mengubah dokumen sebelumnya. (*Format: PDF)</small>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    }); 

    $(function () {
        //Initialize Select2 Elements
        $('.selectGolongan').select2()
        $('.selectBarang').select2()
        $('.selectInstansi').select2()
    });
</script>    
@endsection