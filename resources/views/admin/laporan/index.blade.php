@extends('layouts.master',['activeMenu' => 'laporan'])
@section('title','Cetak Laporan')
@section('breadcrumb', 'Cetak Laporan')
@section('detail_breadcrumb', 'Cetak Laporan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/penitipan/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Penitipan
                        </a> --}}
                        <div class="row" style="margin-bottom: 10px">
                            <form action="{{url('admin/penitipan/cetak')}}" method="GET">
                                <div class="col-md-2">
                                    <select name="jenis" id="" class="form-control">
                                        <option value="">Pilih </option>
                                        <option value="penitipan">Data Penitipan</option>
                                        <option value="mutasi">Data Mutasi</option>
                                        <option value="keluar">Data Barang Keluar</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <input type="date" name="date_1" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <input type="date" name="date_2" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-download"></i>
                                        Cetak Laporan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tablePenitipan').dataTable()
        });
    </script>
@endsection
