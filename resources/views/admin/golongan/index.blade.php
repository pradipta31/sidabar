@extends('layouts.master',['activeMenu' => 'golongan'])
@section('title','Daftar Golongan')
@section('breadcrumb', 'Daftar Golongan')
@section('detail_breadcrumb', 'Daftar Golongan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ isset($golongan) ? 'Edit Data Golongan : '.$golongan->nama_instansi : 'Tambah Data Golongan' }}</h3>
                    </div>
                    <div class="box-body">
                        <form 
                        action="{{ isset($golongan) ? route('update:golongan', $golongan) : route('store:golongan') }}" 
                        method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{ isset($golongan) ? method_field('PUT') : ''}}
                            <div class="form-group">
                                <label for="">Kode</label>
                                <input type="text" name="kode" class="form-control" 
                                value="{{ isset($golongan) ? $golongan->kode : old('kode') }}" placeholder="Masukan Kode">
                            </div>
                            <div class="form-group">
                                <label for="">Nama Golongan</label>
                                <textarea name="nama_golongan" class="form-control" cols="30" rows="5">{{ isset($golongan) ? $golongan->nama_golongan : old('nama_golongan') }}</textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/golongan')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Golongan
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tableBarang" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Golongan</th>
                                        <th>Nama Golongan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($golongans as $golongan)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$golongan->kode}}</td>
                                            <td>{{$golongan->nama_golongan}}</td>
                                            <td>
                                                <a href="{{ route('edit:golongan', $golongan) }}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteGolongan({{ $golongan->id }})">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableBarang').dataTable()
        });

        function deleteGolongan(id){
            swal({
                title: "Anda yakin?",
                text: "Data Golongan akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Golongan yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/golongan/delete/')}}/'+id);
                        $('#formDelete').submit();
                    });
                }
            });
        }
    </script>
@endsection
