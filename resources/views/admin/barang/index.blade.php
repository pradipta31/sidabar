@extends('layouts.master',['activeMenu' => 'barang'])
@section('title','Data Barang')
@section('breadcrumb', 'Data Barang')
@section('detail_breadcrumb', 'Manajemen Data Barang')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ isset($barang) ? 'Edit Data Barang : '.$barang->nama_barang : 'Tambah Data Barang' }}</h3>
                    </div>
                    <div class="box-body">
                        <form 
                        action="{{ isset($barang) ? route('update:barang', $barang) : route('store:barang') }}" 
                        method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{ isset($barang) ? method_field('PUT') : ''}}
                            <div class="form-group">
                                <label for="">Jenis Barang</label>
                                <select name="jenis_barang" class="form-control" value="{{ isset($barang) ? $barang->jenis_barang : old('jenis_barang') }}">
                                    <option value="Di Dalam RUPBASAN" {{ isset($barang) ? $barang->jenis_barang == 'Di Dalam RUPBASAN' ? 'selected' : '' : old('jenis_barang') }}>Di Dalam RUPBASAN</option>
                                    <option value="Di Luar RUPBASAN" {{ isset($barang) ? $barang->jenis_barang == 'Di Luar RUPBASAN' ? 'selected' : '' : old('jenis_barang') }}>Di Luar RUPBASAN</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nama Barang</label>
                                <input type="text" name="nama_barang" class="form-control" 
                                value="{{ isset($barang) ? $barang->nama_barang : old('nama_barang') }}" placeholder="Masukan Nama Barang">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/barang')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Barang
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tableBarang" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($barangs as $barang)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$barang->jenis_barang}}</td>
                                            <td>{{$barang->nama_barang}}</td>
                                            <td>
                                                <a href="{{ route('edit:barang', $barang) }}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteBarang({{ $barang->id }})">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableBarang').dataTable()
        });

        function deleteBarang(id){
            swal({
                title: "Anda yakin?",
                text: "Data Barang akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Barang yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/barang/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
