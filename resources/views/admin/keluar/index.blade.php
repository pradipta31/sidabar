@extends('layouts.master',['activeMenu' => 'pengeluaran'])
@section('title','Data Pengeluaran barang')
@section('breadcrumb', 'Data Pengeluaran barang')
@section('detail_breadcrumb', 'Manajemen Data Pengeluaran barang')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/pengeluaran/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Pengeluaran Barang
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tableKeluar" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Petugas</th>
                                        <th>Kode Barang</th>
                                        <th>Tanggal Pengeluaran</th>
                                        <th>Nama Barang</th>
                                        <th>Status</th>
                                        <th>Detail Barang</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pengeluarans as $pengeluaran)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$pengeluaran->user->nama}}</td>
                                            <td>{{$pengeluaran->penitipan->kode_register}}</td>
                                            <td>{{$pengeluaran->tgl_pengeluaran}}</td>
                                            <td>{{$pengeluaran->penitipan->barang->nama_barang}}</td>
                                            <td>
                                                @if ($pengeluaran->status == 1)
                                                    <span class="label label-primary">Penitipan</span>
                                                @elseif($pengeluaran->status == 2)
                                                    <span class="label label-info">Termutasi</span>
                                                @elseif($pengeluaran->status == 3)
                                                    <span class="label label-danger">Dikeluarkan</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('admin/pengeluaran/detail/'.$pengeluaran->id)}}" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-eye"></i>
                                                    Detail
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{url('admin/pengeluaran/'.$pengeluaran->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deletePengeluaran({{ $pengeluaran->id }})">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableKeluar').dataTable()
        });

        function deletePengeluaran(id){
            swal({
                title: "Anda yakin?",
                text: "Data pengeluaran akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data pengeluaran yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/pengeluaran/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
