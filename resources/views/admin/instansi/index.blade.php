@extends('layouts.master',['activeMenu' => 'instansi'])
@section('title','Daftar Instansi')
@section('breadcrumb', 'Daftar Instansi')
@section('detail_breadcrumb', 'Daftar Instansi')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ isset($instansi) ? 'Edit Data Instansi : '.$instansi->nama_instansi : 'Tambah Data Instansi' }}</h3>
                    </div>
                    <div class="box-body">
                        <form 
                        action="{{ isset($instansi) ? route('update:instansi', $instansi) : route('store:instansi') }}" 
                        method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{ isset($instansi) ? method_field('PUT') : ''}}
                            <div class="form-group">
                                <label for="">Nama Instansi</label>
                                <input type="text" name="nama_instansi" class="form-control" 
                                value="{{ isset($instansi) ? $instansi->nama_instansi : old('nama_instansi') }}" placeholder="Masukan Nama Instansi">
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" cols="30" rows="5">{{ isset($instansi) ? $instansi->deskripsi : old('deskripsi') }}</textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('admin/instansi')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Instansi
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tableBarang" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Instansi</th>
                                        <th>Deskripsi</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($instansis as $instansi)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$instansi->nama_instansi}}</td>
                                            <td>{{$instansi->deskripsi}}</td>
                                            <td>
                                                <a href="{{ route('edit:instansi', $instansi) }}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteInstansi({{ $instansi->id }})">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableBarang').dataTable()
        });

        function deleteInstansi(id){
            swal({
                title: "Anda yakin?",
                text: "Data Instansi akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Instansi yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/instansi/delete/')}}/'+id);
                        $('#formDelete').submit();
                    });
                }
            });
        }
    </script>
@endsection
