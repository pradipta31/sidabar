<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIDABAR | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
    <div class="row" style="margin-top: 20px; margin-bottom: -100px">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="login-logo">
                <img src="{{asset('images/logo.jpeg')}}" alt="" style="width: 20%">
                <br>
                <a href="{{url('login')}}">
                    <b>SISTEM INFORMASI DATA BASAN BARAN RUMAH PENYIMPANAN BENDA SITAAN NEGARA KELAS I DENPASAR</b></a>
                    <br>
                <a href="{{url('login')}}">Login</a>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
<div class="login-box">
    
    <div class="login-box-body">
        <p class="login-box-msg">Silahkan login terlebih dahulu</p>
        <form action="{{route('login')}}" method="POST">
        @csrf
            <div class="form-group has-feedback">
                <input id="username" type="text" class="form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                name="login" value="{{ old('username') ?: old('email') }}" placeholder="Masukan username atau Email" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('username') || $errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>Username/Email atau password salah!</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>Password Tidak boleh kosong!</strong>
                    </span>
                @enderror
            </div>
            <div class="row">
                <div class="col-xs-8">
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block" onclick="loginBtn(this);">
                    Sign In
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
  function loginBtn(d){
    d.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
  }
</script>
</body>
</html>
