{{-- <li class="header">USER NAVIGATION</li>
<li class="treeview {{$activeMenu == 'user' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Petugas</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('user/tambah')}}"><i class="fa fa-plus"></i> Tambah Petugas</a></li>
        <li><a href="{{url('user')}}"><i class="fa fa-circle-o"></i> Data Petugas</a></li>
    </ul>
</li> --}}
<li class="header">MASTER NAVIGATION</li>
<li class="{{$activeMenu == 'barang' ? 'active' : ''}}"><a href="{{url('petugas/barang')}}"><i class="fa fa-clone"></i> <span>Data Barang</span></a></li>

<li class="header">REPLACEMENT NAVIGATION</li>
<li class="treeview {{$activeMenu == 'penitipan' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-hourglass-half"></i>
        <span>Penitipan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('petugas/penitipan/tambah')}}"><i class="fa fa-plus"></i> Tambah Penitipan Baru</a></li>
        <li><a href="{{url('petugas/penitipan')}}"><i class="fa fa-circle-o"></i> Data Penitipan</a></li>
    </ul>
</li>
<li class="treeview {{$activeMenu == 'mutasi' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-exchange"></i>
        <span>Mutasi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('petugas/mutasi/tambah')}}"><i class="fa fa-plus"></i> Tambah Mutasi Baru</a></li>
        <li><a href="{{url('petugas/mutasi')}}"><i class="fa fa-circle-o"></i> Data Mutasi</a></li>
    </ul>
</li>

<li class="header">OUT TOOLS NAVIGATION</li>
<li class="treeview {{$activeMenu == 'pengeluaran' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-trash"></i>
        <span>Barang Keluar</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('petugas/pengeluaran/tambah')}}"><i class="fa fa-plus"></i> Tambah Barang Keluar</a></li>
        <li><a href="{{url('petugas/pengeluaran')}}"><i class="fa fa-circle-o"></i> Data Barang Keluar</a></li>
    </ul>
</li>
<li class="{{$activeMenu == 'laporan' ? 'active' : ''}}"><a href="{{url('petugas/laporan')}}"><i class="fa fa-newspaper-o"></i> <span>Cetak Laporan</span></a></li>